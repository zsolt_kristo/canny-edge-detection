#include<opencv2/opencv.hpp>
#include<iostream>
#include<chrono>
#include<conio.h>           // may have to modify this line if not using Windows

using namespace std;
using namespace std::chrono;

int main(int argc, char* argv[])
{
	try
	{
		//reading source images into memory
		vector<cv::String> fn;
		//cv::glob("../../source_images/256x256/*.jpg", fn, false);
		//cv::glob("../../source_images/512x512/*.jpg", fn, false);
		//cv::glob("../../source_images/1024x1024/*.jpg", fn, false);
		//cv::glob("../../source_images/1600x1200/*.jpg", fn, false);
		//cv::glob("../../source_images/1920x1080/*.jpg", fn, false);
		cv::glob("../../source_images/3840x2160/*.jpg", fn, false);

		high_resolution_clock::time_point t1, t2;

		vector<cv::Mat> imagesOriginal;
		size_t count = fn.size();			 //number of image files in images folder
		for (size_t i = 0; i < count; i++) {
			cout << "Reading image " << i + 1 << " of " << count << "\n";
			imagesOriginal.push_back(imread(fn[i], CV_LOAD_IMAGE_GRAYSCALE)); // reading images in gryscale format
		}

		cout << "\n\n-------------------- Reading images done. Starting to process them. --------------------\n\n";

		t1 = high_resolution_clock::now();			//start time
		for (size_t i = 0; i < count; i++) {

			//variables that holds images
			cv::Mat imgOriginal = imagesOriginal[i];
			cv::Mat imgCanny;           // Canny edge image
			cv::Mat imgBlurred;

			cout << "Processing image " << i + 1 << " of " << count << "\n";

			//t1 = high_resolution_clock::now();
			cv::GaussianBlur(imgOriginal,          // input image
				imgBlurred,                         // output image
				cv::Size(5, 5),                     // smoothing window width and height in pixels
				3);

			cv::Canny(imgBlurred,           // input image
				imgCanny,                   // output image
				128.0,                         // low threshold
				255.0);                       // high threshold
			//t2 = high_resolution_clock::now();

			std::stringstream imgTitle;
			imgTitle << "./img_canny/image_" << i + 1 << ".jpg";
			cv::imwrite(imgTitle.str(), imgCanny);
		}
		t2 = high_resolution_clock::now();			//end time
		auto duration = duration_cast<microseconds>(t2 - t1).count();			// calculate duration

		cout << "\n\n --- Operation done! --- \n\n";
		cout << "It took " << duration / 1000.0 << " milliseconds.\n";
		int a;
		cin >> a;
	}
	catch (const cv::Exception& ex)
	{
		std::cout << "Error: " << ex.what() << std::endl;
	}
	return(0);
}
