#include <iostream>
#include "opencv2/opencv.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;
using namespace std;

int main(int argc, char* argv[])
{
	try
	{
		//reading source images into memory
		vector<cv::String> fn;
		//cv::glob("../../source_images/256x256/*.jpg", fn, false);
		//cv::glob("../../source_images/512x512/*.jpg", fn, false);
		//cv::glob("../../source_images/1024x1024/*.jpg", fn, false);
		//cv::glob("../../source_images/1600x1200/*.jpg", fn, false);
		//cv::glob("../../source_images/1920x1080/*.jpg", fn, false);
		cv::glob("../../source_images/3840x2160/*.jpg", fn, false);

		vector<cv::Mat> imagesOriginal;
		size_t count = fn.size();			 //number of image files in images folder

		cudaEvent_t start, stop;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);
		 
		for (size_t i = 0; i < count; i++) {
			cout << "Reading image " << i + 1 << " of " << count << "\n";
			imagesOriginal.push_back(imread(fn[i], CV_LOAD_IMAGE_GRAYSCALE)); // reading images in gryscale format
		}

		cout << "\n\n-------------------- Reading images done. Starting to process them. --------------------\n\n";

		// variables that holds images
		cv::Mat srcHost;
		cuda::GpuMat dst, src;
		cv::Mat resultHost;

		cv::Ptr<cv::cuda::Filter> gaussian_filter = cv::cuda::createGaussianFilter(
			CV_8UC1,			//input image format -- defined by imread
			CV_8UC1,			// oputput image format -- defined by imread
			cv::Size(5, 5),		// size of Gauss matrix
			3);					// sima value

		cv::Ptr<cv::cuda::CannyEdgeDetector> canny_edge = cv::cuda::createCannyEdgeDetector(
			128.0,			// low threshold
			255.0,			// high treshold
			3,				// sigma value
			false);

		cudaEventRecord(start);			//start time
		for (size_t i = 0; i < count; i++) {
			srcHost = imagesOriginal[i];

			cout << "Processing image " << i + 1 << " of " << count << "\n";

			//cudaEventRecord(start);

			src.upload(srcHost);
			gaussian_filter->apply(src, dst);
			canny_edge->detect(src, dst);
			dst.download(resultHost);

			//cudaEventRecord(stop);
			//cudaEventSynchronize(stop);

			std::stringstream imgTitle;
			imgTitle << "./img_canny/image_" << i + 1 << ".jpg";
			cv::imwrite(imgTitle.str(), resultHost);
		}
		cudaEventRecord(stop);			// end time
		cudaEventSynchronize(stop);			// syncronisation
		
		float duration = 0;
		cudaEventElapsedTime(&duration, start, stop);			// calculate duration

		cout << "\n\n --- Operation done! --- \n\n";
		cout << "It took " << duration << " milliseconds.\n";
		int a;
		cin >> a;
	}
	catch (const cv::Exception& ex)
	{
		std::cout << "Error: " << ex.what() << std::endl;
	}
	return 0;
}